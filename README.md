<p align="center" class="text-center">
  <a href="https://digitales-wartezimmer.org/" target="blank"><img src="https://i.imgur.com/z4zeQQO.png" width="320" alt="Digitales Wartezimmer Logo" /></a>
</p>
  
<p align="center" class="text-center">A digital solution for simple and efficient exchange of information between health authorities and citizens for rapid containment of infectious diseases.</p>
<p align="center" class="text-center">
<a href="https://www.gnu.org/licenses/gpl-3.0" target="_blank"><img src="https://img.shields.io/badge/License-GPLv3-brightgreen.svg" alt="License GPLv3"></a>
</p>

> :warning: This project is sadly not active anymore and unmaintained. For more info visit https://digitales-wartezimmer.org. Should you have questions regarding the codebase feel free to send an [E-mail](mailto:digitales-wartezimmer@bernhardwittmann.com).

# Digitales Wartezimmer - Documentation

#### :rocket: [https://digitales-wartezimmer.gitlab.io/documentation/](https://digitales-wartezimmer.gitlab.io/documentation/) 

With our digital solution, all relevant information of COVID-19 contact persons will be digitally captured and transferred to the systems used by the responsible health authorities to track contact persons.

By digitizing the transmission of COVID-19 contact person data, we want to help health authorities break the chain of infection quickly. At the same time, the COVID-19 contact person solution should provide a fast and user-friendly way to contact his/her health authority. 

This repository contains miscellaneous documentation and diagrams.

## Using this repository

This repository is available as a [website](https://digitales-wartezimmer.gitlab.io/documentation/).

If you should want to edit this file use the following steps after cloning this repository.

Install dependencies

```bash
$ npm install
```

Load page in development mode. The page will be available at http://localhost:8080/documentation/

```bash
$ npm run docs:dev
```

Since this is based on [Vuepress](https://vuepress.vuejs.org/) refer to its documentation for further information.

## Support

Digitales Wartezimmer is a GPLv3 open source project. It is powered by our awesome [Team](https://digitales-wartezimmer.org/project). If you'd like to join us, please contact [info@digitales-wartezimmer.org](mailto:digitales-wartezimmer.org)

## Stay in touch

- Website - [https://digitales-wartezimmer.org](https://digitales-wartezimmer.org)
- Twitter - [@Dig_Wartezimmer](https://twitter.com/Dig_Wartezimmer)
- Facebook - [DigitalesWartezimmer](https://www.facebook.com/DigitalesWartezimmer/)
- LinkedIn - [digitales-wartezimmer](https://www.linkedin.com/company/digitales-wartezimmer/)

## License

  Digitales Wartezimmer is [GPLv3 licensed](LICENSE).
