---
title: Techstack
---

# Techstack

This page describes the used technologies and frameworks.

## Backend

* [NodeJS](https://nodejs.org/) Run JavaScript as backend process
* [Typescript](https://www.typescriptlang.org/) Typing for JavaScript
* [NestJS](https://nestjs.com/) Framework for building JavaScript APIs
* [Jest](https://jestjs.io/) Testing framework
* [MongoDB](https://www.mongodb.com/) Database
* [Redis](https://redis.io/) Database used for Session Storage and Caching
* [Sentry](https://sentry.io) Error Tracking

## Frontend

* [Vue.js](https://vuejs.org/) Frontend JavaScript Framework
* [NuxtJS](https://nuxtjs.org/) Application Framework on top of Vue.js
* [Typescript](https://www.typescriptlang.org/) Typing for JavaScript
* [Jest](https://jestjs.io/) Testing framework
* [Cypress](https://www.cypress.io/) End-to-End testing framework
## Other

* [GitLab](https://gitlab.com) for Git Hosting and Continouos Integration
* [Docker](https://www.docker.com/) for Deployment
* [Strapi](https://strapi.io/) for an internal content tool
* [UpTimeRobot](https://uptimerobot.com/) for [Monitoring](./monitoring)
