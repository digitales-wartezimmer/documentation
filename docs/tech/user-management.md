---
title: User Management
---


# User Management

This page explains how users of the API can be configured.

## Available Roles

There exist different roles with different permissions, to prevent access to resources that are not necessary for the intended use. Which resources are available for which role can be found in the [API-Documentation](../tech/api-documentation). As a general rule of thumb, the roles are listed below in ascending order of priviliges.

| Role | Description |
|------|-------------|
| Monitor      | Role that is allowed to request health endpoints for automatic checking of the status of the :API: |
| User     | Role for an end user or client that is used directly by end users | 
| HealthOffice     | Role intended for use of health offices, to configure data related to a specific health office | 
| Admin     | Admin Role to maintain and configure the application | 

## Managing users

You can handle and manage users with direct [Database Access](../guides/db-connection).

### List users

Return a list of all available users

```
db.users.find();
```

### Create User

Create a random token. You can use the following command to generate a random string.

```
openssl rand -hex 16
```

You can then use the token and set the desired role, as well as a fitting name.

```
db.users.insert({ name: 'Monitor', token: 'cr3B4dLRmPNs9nAiCZuR44gRT6pjGEm7', role: 'monitor' })
```

### Delete User

Delete a user by given name

```
db.users.remove({ name: 'Monitor' })
```

### Update a user

You can also update the values of a user which is being identified by its name with the following command.

```
db.users.update({ 'name': "Monitor" },{ $set: { 'name': 'Admin', 'token': 'abc123', role: 'admin' }}});
```