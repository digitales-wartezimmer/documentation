---
title: Testing
---

# Testing

This page describes the Testing-Strategy for validating the functionality of the application.

The testing strategy orients itself after the Testing pyramid.

![Testing pyramid](../assets/testing_pyramid.png)

*Source: [https://alm.parasoft.com/hs-fs/hubfs/New_Pages/testing_pyramid.png?width=800&name=testing_pyramid.png](https://alm.parasoft.com/hs-fs/hubfs/New_Pages/testing_pyramid.png?width=800&name=testing_pyramid.png)*

## Unit-Tests

Unit tests are inteded to granular test single components and functions. The percentage of lines of code covered by unit tests can be found in the badges below.

![](https://img.shields.io/gitlab/coverage/digitales-wartezimmer/frontend/main?style=for-the-badge&label=Coverage%20Frontend)
![](https://img.shields.io/gitlab/coverage/digitales-wartezimmer/gateway/main?style=for-the-badge&label=Coverage%20Backend)

## End-to-End tests

End-to-End tests simulate a user using the UI of the application and validating the general use cases of the applications.

The test results and recordings can be viewed on the [Cypress Dashboard](https://dashboard.cypress.io/projects/e5vnnv/).

## Manual tests

Before releasing new features or fixes to the [production environment](./environments#production) those changes are validated manually in the [staging environment](./environments#staging).