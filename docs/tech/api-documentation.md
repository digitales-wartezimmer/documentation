---
title: API Documentation
---

# API Documentation

The detailed documentation of the :API: and all available API - endpoints can be found on [https://api.digitales-wartezimmer.org/docs](https://api.digitales-wartezimmer.org/docs). The API-documentation for the [staging environment](./environments#staging) can be found on [https://staging.api.digitales-wartezimmer.org/docs](https://staging.api.digitales-wartezimmer.org/docs)