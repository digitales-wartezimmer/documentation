---
title: Client Authentication
---


# Client Authentication

This page outlines how the [Session based Authentication](../guides/authentication#session-authentication) is used in the frontend of the application. 

Upon payload a session is being requested and then used for subsequent requests. This way client applications cannot freely consume the :API: but need to be authorized. 

The token that belongs to the client's [user](./user-management) and sent with the authentication request does only exist on the Web-Server and is not delivered to the user. Thus, this token cannot be directly obtained and misused.

The session cookie expiration duration is only 30 minutes. This is rather short to lessen the danger of sessions being stolen.


@startuml

skinparam sequence {
    ArrowColor #21ABAB
    ActorBorderColor #21ABAB
    LifeLineBorderColor #21ABAB
    ParticipantBorderColor #21ABAB
    ParticipantBackgroundColor white
    ParticipantFontColor #002F33
}

actor "User (Browser)" #21ABAB
participant "Web-Server"
participant "API"

"User (Browser)" -> "Web-Server" : Request Page (Request /)
"Web-Server" -> "API" : /auth/authenticate with Token
"API" --> "Web-Server" : Session Cookie
"Web-Server" --> "User (Browser)" : Page including Session Cookie
...
"User (Browser)" -> "Web-Server" : Request of any Api-Ressource including Session Cookie
"Web-Server" -> "API" : Request of any Api-Ressource including Session Cookie
"API" --> "Web-Server" : Response
"Web-Server" --> "User (Browser)" : Response
...
"User (Browser)" -> "Web-Server" : Request including Api-Ressource without (expired) Session Cookie
"Web-Server" -> "API" : Request of any Api-Ressource without Session Cookie
"API" --> "Web-Server" : Unauthorized Response
"Web-Server" --> "User (Browser)" : Unauthorized Response
"User (Browser)" -> "User (Browser)" : Notification about expired Session with action to reload page
@enduml