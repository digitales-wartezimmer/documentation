---
title: Deployment
---

# Deployment
## Automatic Deployment

As a Continouos Integration pipeline Gitalb CI is configured to automatically deploy the most recent changes depending on the branch. The following table shows which branch that is being pushed to triggers a deployment to which environment.

| Branch        | Environment           |
| ------------- |-------------|
| main      | production |
| staging     | staging      |

## Manual Deployment

When logged in to the server you can manage the running services

Pull changes from the prebuilt docker images

| Application        | Environment           | Package Url |
| ------------- |-------------|-------|
| frontend      | production |  registry.gitlab.com/digitales-wartezimmer/frontend:latest |
| frontend     | staging      |  registry.gitlab.com/digitales-wartezimmer/frontend:staging |
| backend      | production |  registry.gitlab.com/digitales-wartezimmer/gateway:latest |
| backend     | staging      |  registry.gitlab.com/digitales-wartezimmer/gateway:staging |

```bash
docker pull registry.gitlab.com/digitales-wartezimmer/gateway:staging
docker-compose up -d --no-deps --force-recreate gateway-staging
```

Alternatively you can manually interact with the running services using `docker-compose` commands.

```bash
docker-compose down # Stop all Services
docker-compose up -d # Start all Services
```
