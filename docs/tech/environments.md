---
title: Environments
---

# Environments

This app has two different instances or environments.
## Production

The Production environment is the main environment used by the real users hand handling real data. Thus, this is the most critical environment.

The production app is available at [digitales-wartezimmer.org](https://digitales-wartezimmer.org). The :API: runs on [api.digitales-wartezimmer.org](https://api.digitales-wartezimmer.org).

## Staging

The Staging environment is inteded for testing use. It contains test data, like the two demo health offices (`00000` and `00001`) to trigger the demo workflows [SORMAS](../concepts/workflows#sormas) and [IRIS](../concepts/workflows#iris). 

The production app is available at [staging.digitales-wartezimmer.org](https://staging.digitales-wartezimmer.org). The :API: runs on [staging.api.digitales-wartezimmer.org](https://staging.api.digitales-wartezimmer.org).

The staging environment is protected by basic authentication to prevent indexing by search engines. Use username `dw` and password `dw`.