---
title: Monitoring
---


# Monitoring

[UptimeRobot](https://uptimerobot.com/) checks the application and :API: at regulary intervals whether the app is up and running. In case the app is not running or crashed it will notify the maintainers.

The current status and recent incidents are displayed on the Status Page located at [status.digitales-wartezimmer.org](https://status.digitales-wartezimmer.org).

The current status is also displayed in the badges below

![](https://img.shields.io/uptimerobot/status/m786427079-170aecc04d13e1f85353818b?label=status%20production&style=for-the-badge)
![](https://img.shields.io/uptimerobot/status/m786427077-89c10f1f5ab73a6ba0a5dbe4?label=status%20staging&style=for-the-badge)

![](https://img.shields.io/uptimerobot/status/m786427046-00770a7245d443df83effd1a?label=status%20API%20production&style=for-the-badge)
![](https://img.shields.io/uptimerobot/status/m786427050-9d1cb37b234488bcd8a373c2?label=status%20API%20production&style=for-the-badge)