---
title: Infrastructure
---


# Infrastructure

The infrastructure of Digitales Wartezimmer consists of two servers: `dwz-front` and `dwz-back`.

The `dwz-front` server acts as a [demilitarized zone](https://en.wikipedia.org/wiki/DMZ_(computing)) and thus, the `dwz-back` server is not accessible from the outside and only from the `dwz-front` server.

@startuml

skinparam sequence {
    ArrowColor #21ABAB
    ActorBorderColor #21ABAB
    LifeLineBorderColor #21ABAB
    ParticipantBorderColor #21ABAB
    ParticipantBackgroundColor white
    ParticipantFontColor #002F33
}

skinparam interface {
  backgroundColor #21ABAB
  borderColor #21ABAB
}

skinparam component {
  FontSize 13
  BackgroundColor<<Apache>> Red
  BorderColor<<Apache>> #21ABAB
  FontName Courier
  BorderColor #21ABAB
  BackgroundColor white
  ArrowFontName Impact
  ArrowColor #21ABAB
  ArrowFontColor #21ABAB
}

Internet - [dwz-front]
[dwz-front] - [dwz-back]

@enduml

The following services are running on `dwz-front`:

* Frontend
* Staging Frontend
* Backend
* Staging Backend

The following services are running on `dwz-back`:

* MongoDB
* Redis database

Read the [SSH Connection guide](../guides/ssh-connection) to learn how to connect to the servers.