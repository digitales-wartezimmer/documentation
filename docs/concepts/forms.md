---
title: Forms
---


# Forms

The Digitales Wartezimmer bundles multiple use cases in one platform. These use cases are separate and independent so they are implemented by use-case specific forms. Each use-case is being implemented by a specific form.

The following forms are currently implemented on the platform.

## Index Case

This form is intended to be used by people who have tested positive for COVID-19. In that case such :IndexPerson:Indexpersons: are required to inform the health offices about recent :ContactPerson:contactpersons: to investigate possible contacts and infections.

This form is available at [digitales-wartezimmer.org/index-case](https://digitales-wartezimmer.org/index-case).

The following data is being collected.

@startuml
skinparam activity {
  ArrowColor #21ABAB
  StartColor #21ABAB
  BarColor #21ABAB
  EndColor #21ABAB
  BackgroundColor white
  BorderColor #21ABAB
  DiamondBackgroundColor white
  DiamondBorderColor #21ABAB
}

start
:zip code;
:firstname and lastname of index person;
:case id / tan //(optional)//;

repeat
  :firstname and lastname of contact person;
  :phone and email address //(at least one)//;
  :address: street, house number, zip code, city //(optional)//;
repeat while ()

:personal message;

stop

@enduml

## Registration As Contact

If a :ContactPerson: has been notified that it had contact with an infected person, this person can notify their local health office proactively.

This form is available at [digitales-wartezimmer.org/registration-as-contact](https://digitales-wartezimmer.org/registration-as-contact)

The following data is being collected.

@startuml
skinparam activity {
  ArrowColor #21ABAB
  StartColor #21ABAB
  BarColor #21ABAB
  EndColor #21ABAB
  BackgroundColor white
  BorderColor #21ABAB
  DiamondBackgroundColor white
  DiamondBorderColor #21ABAB
}

start
:zip code;
:firstname and lastname;
:birthday;
:gender;
:email address //(optional)//;
:phone;
:street and house number;
:zip code;
:city;

if (source of contact) then (personal contact)
  :firstname and lastname of contact //(optional)//;
  :phone of contact //(optional)//;
  :email address of contact //(optional)//;
  :location of contact //(optional)//;
  :date of contact;
  :closeness of contact;
  :justification for closeness of contact;
elseif (source of contact) then (cwa)
  :count of risk encounters;
  :date of last risk encounter;
else (establishment)
  :location of contact //(optional)//;
  :date of contact;
endif

if (symptoms) then (yes)
  :description of symptoms;
  :begin of symptoms;
else (no)
endif

if (precondition immune system) then (yes)
  :description of precondition;
else (no)
endif

if (precondition heart) then (yes)
  :description of precondition;
else (no)
endif

if (gender) then (female, diverse)
    if (pregnancy) then (yes)
    :week of pregnancy;
    else (no)
    endif
else (male)
endif

:personal message;

stop

@enduml

## Travel Return

::: warning DEPRECATED
This form is not active anymore, since the use case has been covered by [Digitale Einreiseanmeldung](https://www.einreiseanmeldung.de/#/).
:::

The Travel Return form allows users who are returning to Germany from a virus risk area to fulfill the requirement to notify their local health office of their return. 

This form is available at [digitales-wartezimmer.org/travel-return](https://digitales-wartezimmer.org/travel-return)