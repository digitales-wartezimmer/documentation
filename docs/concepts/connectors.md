---
title: Connectors
---


# Connectors

Connectors hold authentication data for external services. They are used mostly for storing the connection details of different :SORMAS: instances. A connector is unqiue for a health office and the consuming service. The password that is stored with the connector is encrypted on the application level before being stored to the database to protect the data at rest.

Learn more about the interaction with connectors in the [connector configuration guide](../guides/connector-configuration).

@startuml

skinparam sequence {
    ArrowColor #21ABAB
    ActorBorderColor #21ABAB
    LifeLineBorderColor #21ABAB
    ParticipantBorderColor #21ABAB
    ParticipantBackgroundColor white
    ParticipantFontColor #002F33
}

actor "Health Office" #21ABAB
actor "User" #21ABAB
participant "API"
database DB #FFFFFF
database SORMAS #FFFFFF

group Adding of Connector data
    "Health Office" -> "API" : POST Request /connectors with credentials in request body
    "API" --> "API" : Encryption of credentials with local DB secret
    "API" -> "DB" : Store encrypted credentials in database
    "API" --> "Health Office" : Success - Response
end

...

group Usage of Connector data
    "User" -> "API" : Sends data
    "API" -> "DB" : Retrieve relevant connector configuration
    "DB" --> "API" : encrypted credentials
    "API" --> "API" : decrypt credentials with local DB secret
    "API" -> "SORMAS" : Send data with correct credentials
    "API" --> "User" : Sucess - Response
end
@enduml