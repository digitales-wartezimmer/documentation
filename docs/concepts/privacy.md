---
title: Privacy
---

# Privacy

This page outlines the approach the Digitales Wartezimmer takes on the topic of Privacy.

## Data sparingness

In general, Digitales Wartezimmer tries to use and collect as little as possible of data.
Ee do not use any Error tracking in the frontend, but only in the backend to lessen the risk of data leaks. And where we use error tracking we configured it to not include any personal data. Also we do not have any analytics tools installed in the application and only derive statistics from the server logs.

## Storing no personal data

We also do not store the data users are entering in the forms and only sending them onwards to [SORMAS](./workflows#sormas) or [IRIS](./workflows#iris). Thus, the data is only passing through our backend and not stored for any statistical evaluations or other use.

When the data is sent to :IRIS: the data is being end-to-end encrypted, so neither we nor any attacker can even read the data. Only the responsible health office can decrypt the data. This is only the case for the [IRIS workflow](./workflows#iris).

## Open Source

Our whole source code is open source and also this documentation is public. This allows every person, users, and security researchers to get a full picture on what and how the Digitales Wartezimmer is doing. This enables us to receive and respond to comments and requests from the public. 