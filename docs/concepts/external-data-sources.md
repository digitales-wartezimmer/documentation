---
title: External Data Sources
---

# External Data Sources

The application depends on data from external sources

## RKI PLZTool

The [RKI PLZTool](https://tools.rki.de/PLZTool/en-GB) is used to retrieve the latest data from the RKI to map the zip code of a user to the health office responsible for this user. 

## PLZ Info Tool

For information about the zip code the user supplied the dataset `georef-germany-postleitzahl` is being used that provides, geolocation, city and state information about the entered zip code. This dataset is being provided by [opendatasoft](https://public.opendatasoft.com) and used for prefilling the city name based on the zip code.