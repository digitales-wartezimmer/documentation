---
title: Workflows
---

# Workflows

A workflow represents a way on how the data of any [Form](./forms) can be transmitted to a health office. These workflows can be different for each form and can be configured on a per health office bases. This workflow configuration is explained more depth in the [workflow configuration management guide](../guides/workflow-configuration).

Currently there are three different workflows implemented.

## E-Mail

This workflow does not send the data directly, instead it generates an email text that contains the relevant data. This email can also be supplied with a csv file that is also generated with this workflow and contains the data to be imported in other software systems like SORMAS.

**Important**: This workflow does not send the email/data directly, but generates the email for the users to send themselves.

This workflow is also used, if no workflow is specified at all for the given health office. It is also used as a fallback if another workflow errors.

@startuml

skinparam sequence {
    ArrowColor #21ABAB
    ActorBorderColor #21ABAB
    LifeLineBorderColor #21ABAB
    ParticipantBorderColor #21ABAB
    ParticipantBackgroundColor white
    ParticipantFontColor #002F33
}

participant "API"
actor "User" #21ABAB
participant "Health Office"

"User" -> "API": Send form data
"API" --> "API": Pregenerate email text and csv attachment
"API" --> "User": Respond with pregenerated email
"User" -> "Health Office": Send pregenerated email
@enduml

## SORMAS

Instead of sending the data via email which requires manual interaction on the side of the health office, the :SORMAS: Workflow tries to push the data directly into :SORMAS:. :SORMAS: is the main software to manage incidents of contacts and is the main database for positive cases and their :ContactPerson:contactpersons:. Thus, this workflow directly sends the data there with the :API: provided by :SORMAS:.

This workflow uses [connectors](./connectors) to retrieve the authentication details for the connected :SORMAS: instances.

The :SORMAS: workflow can be tested by entering `00000` as zip code on the [Index case form](../forms#index-case) on the [staging environment](../tech/environments#staging).

@startuml

skinparam sequence {
    ArrowColor #21ABAB
    ActorBorderColor #21ABAB
    LifeLineBorderColor #21ABAB
    ParticipantBorderColor #21ABAB
    ParticipantBackgroundColor white
    ParticipantFontColor #002F33
}

actor "User" #21ABAB
participant "API"
database "SORMAS" #FFFFFF

"User" -> "API": Send form data
"API" -> "SORMAS": Send data
"SORMAS" --> "API": Successful response
"API" --> "User": Successful response
@enduml

### SORMAS Entities created

This section describes what entities are created in the :SORMAS: instance for each form.

Currently only the [Index case form](../forms#index-case) is implemented to push data directly to :SORMAS:.

#### Index Case Form

When submitting the data of the [Index case form](../forms#index-case) there are separate entities being created.

The :IndexPerson: is identified by the Case ID from :SORMAS:. This CASE ID has to be supplied by the user in the form as well, to associate the data with the correct :SORMAS: data.

##### Contact Person

First of all for every :ContactPerson: there is a person created in :SORMAS: with the given information (Name, Email, Phone, Address) and also a contact entity that ties the :ContactPerson: to the :IndexPerson: as a possible contact and maybe also infection

##### Contact investigation task

To support task based workflows within :SORMAS: for every :ContactPerson: there is also a task being created that prompts the workers at the health office that this :ContactPerson: needs to be investigated for their possible infection risk. This task also contains the contact information for easier access for the health office workers.

##### Contacts reported task

Additionally, it is important for the health office workers to know, when an :IndexPerson: has obeyed their duty to send the infomration about the :ContactPerson:Contactpersons:. Therefore, when the data of the [Index case form](../forms#index-case) is being submitted, a task is created in sormas associated with the :IndexPerson: to signify the health office workers that this has been completed. This task is created directly with the *completed* status.

## IRIS

Instead of pushing the data directly to :SORMAS:, the :IRIS: workflow allows to send the data to the :IRIS: gateway that then sends the data into :SORMAS:. Thus, the Digitales Wartezimmer does not hold authentication details for several :SORMAS: instances and it is easier to incorporate new Health offices.

The :IRIS: workflow will also create the Contact person and contact persons in the :SORMAS: instance as described above. However, using the :IRIS: workflow does render the possibility to create corresponding tasks as outlined above in the :SORMAS: workflow unfeasible, since no direct connection to :SORMAS: exists. Nevertheless, there appears to be some plans to create similar tasks in :SORMAS: within the :IRIS: team as well. Yet, the similarity and existence in the future cannot be guaranteed as of right now.

The :IRIS: workflow can be tested by entering `00001` as zip code on the [Index case form](../forms#index-case) on the [staging environment](../tech/environments#staging).

@startuml

skinparam sequence {
    ArrowColor #21ABAB
    ActorBorderColor #21ABAB
    LifeLineBorderColor #21ABAB
    ParticipantBorderColor #21ABAB
    ParticipantBackgroundColor white
    ParticipantFontColor #002F33
}

actor "User" #21ABAB
participant "API"
database "IRIS" #FFFFFF
database "SORMAS" #FFFFFF

"User" -> "API": Send form data
"API" -> "IRIS": Send data
"IRIS" -> "SORMAS": Send data
"IRIS" --> "API": Successful response
"API" --> "User": Successful response
@enduml
