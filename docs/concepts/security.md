---
title: Security
---

# Security

This page outlines the approach the Digitales Wartezimmer takes on the topic of Security.

## Data sparingness

In general, Digitales Wartezimmer tries to use and collect as little as possible of data. Therefore, we also do not store the data users are entering in the forms and only sending them onwards to [SORMAS](./workflows#sormas) or [IRIS](./workflows#iris). 

Furthermore, we do not use any Error tracking in the frontend, but only in the backend to lessen the risk of data leaks. And where we use error tracking we configured it to not include any personal data. Also we do not have any analytics tools installed in the application and only derive statistics from the server logs.

Since we do not store any personal data entered by the users, this reduces the risk of stealing such data significantly.

## Encryption

The only real relevant data we store in any database are the [connectors](./connectors) who hold the access details for the :SORMAS: instances. To protect this data at rest, the password is being encrypted on the application level before being saved to the database.

When the data is sent to :IRIS: the data is being end-to-end encrypted, so neither we nor any attacker can read the data. Only the responsible health office can decrypt the data. This is only the case for the [IRIS workflow](./workflows#iris).

## Authentication

To prevent misuse of our :API:, users who try to connect and consume our :API: need to authenticate and provide a valid token. Learn more about the available authentication schemes in the [Authentication Guide](../guides/authentication).

The data in transport, which a user sends the data to our backend or then the backend sends the data o :SORMAS: or :IRIS:, is being secured by using `HTTPS`. This way the data cannot be wiretapped, since the traffic is encrypted.

## Rate Limiting

To prevent [denial-of-service attacks](https://en.wikipedia.org/wiki/Denial-of-service_attack) or brute force attacks on our :API: the endpoints are rate limited. This way the amount of requests that can be sent the the :API: is limited by ip address. This limits the possibility to cause excessive load and slow down brute force attacks.

## Open Source

Our whole source code is open source and also this documentation is public. This allows every person, users, and security researchers to get a full picture on what and how the Digitales Wartezimmer is doing. This enables us to receive and respond to comments and requests from the public. 

## Automated Security Scanning

The codebase is automatically scanned for vulnerabilities with [Snyk](https://snyk.io/).