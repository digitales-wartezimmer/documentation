---
home: true
heroImage: /icon.png
heroText: Digitales Wartezimmer - Documentation
tagline: A digital solution for simple and efficient exchange of information between health authorities and citizens for rapid containment of infectious diseases.
actionText: Get Started →
actionLink: getting-started/
footer: Digitales Wartezimmer
---

::: warning ⚠️ Warning ⚠️
This project is sadly not active anymore and unmaintained. For more info visit [digitales-wartezimmer.org](https://digitales-wartezimmer.org). Should you have questions regarding the codebase feel free to send an [E-mail](mailto:digitales-wartezimmer@bernhardwittmann.com).
:::

<div class="features">
  <div class="feature">
    <h2>Concepts</h2>
    <p>Get an overview of the concepts and general building blocks</p>
    <a href="concepts/" class="nav-link button">Concepts</a>
  </div>
  <div class="feature">
    <h2>Guides</h2>
    <p>Collection of different tutorials and how-to guides</p>
    <a href="guides/" class="nav-link button">Guides</a>
  </div>
  <div class="feature">
    <h2>Tech</h2>
    <p>Deep dive into technical aspects</p>
    <a href="tech/" class="nav-link button">Tech</a>
  </div>
</div>

