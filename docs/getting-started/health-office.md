---
title: For Health Offices
---


# Getting Started as a Health Office

To use Digitales Wartezimmer ask your *users* to use the Digitales Wartezimmer by letting them know the link. You can either provide the default url [https://digitales-wartezimmer.org](https://digitales-wartezimmer.org) or the direct url to one of the available [forms](../concepts/forms).

By default, your users will then send the data by [email](../concepts/workflows#e-mail). If you want to have the data directly in your software system like :SORMAS:, you would need to configure the [SORMAS workflow](../concepts/workflows#sormas). For :IRIS: the [IRIS workflow](../concepts/workflows#iris) needs to be set up.

If you want to connect your health office now, please contact the team via [email](mailto:info@digitales-wartezimmer.org) and we will help you get started.

Should you have more questions regarding privacy and security, please refer to the documentation about our [privacy approach](../concepts/privacy) and [security practices](../concepts/security).
