---
title: Getting Started
---

# Getting Started

## [Motivation](motivation)

We recommend reading the motivation and background why the Digitales Wartezimmer exists and what problem it solves.

<a href="motivation" class="nav-link button">Motivation</a>

<div class="features">
  <div class="feature">
    <h2>For Users</h2>
    <p>Are you an end-user? See how you can use Digitales Wartezimmer</p>
    <a href="users" class="nav-link button">Getting started as a user</a>
  </div>
  <div class="feature">
    <h2>For Health Offices</h2>
    <p>How to get started in utilizing the Digitales Wartezimmer for your health office</p>
    <a href="health-office" class="nav-link button">Getting started as a Health Office</a>
  </div>
  <div class="feature">
    <h2>For Developers</h2>
    <p>Deep dive into technical aspects and how to setup the application</p>
    <a href="developer" class="nav-link button">Getting started as a Developer</a>
  </div>
</div>