---
title: For Developers
---


# Getting Started as a Developer

> *Talk is cheap. Show me the code*
>
> Linus Torvalds

You can find our code over at [GitLab](https://gitlab.com/digitales-wartezimmer/).

Nevertheless, we recommend reading about the [concepts](../concepts) first.

If you want to interact with our API, please refer to the [API-documentation](../tech/api-documentation) and [Authentication guide](../guides/authentication).