---
title: For Users
---

# Getting Started as a User

To send your personal information to your local health office you can use the Digitales Wartezimmer online in your browser. We will not store any data and only send them to your Health Office.

<a href="https://digitales-wartezimmer.org" class="nav-link button">Visit Digitales Wartezimmer</a>

Should you have concerns about what happens with your data, we recommend reading about our [Security](../concepts/security) and [Privacy](../concepts/privacy) concepts.