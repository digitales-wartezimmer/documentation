---
title: Motivation
---

# Motivation

In the current pandemic it is critical to track and investigate chains of contact. Without an efficient contact tracing, these chains cannot be interrupted and infection clusters cannot be contained.

## Current contact tracing process

In general, when a person tests positive for Covid-19 this person is required to inform the local health office about recent contacts with other persons. This way these possibly infected persons can be contacted and quarantined if necessary.

Thus, retrieving the list of contact persons from the :IndexPerson: is critical and now usually done by having a worker at the health office call the :IndexPerson: and ask for the data, and noting down the names, contact information and address manually, which tends to be quite a tedious process. This data has then to be manually entered into the :SORMAS: software for managing and tracking those contacts.

The process diagram below outlines the current progress.

@startuml
skinparam activity {
  ArrowColor #21ABAB
  StartColor #21ABAB
  BarColor #21ABAB
  EndColor #21ABAB
  BackgroundColor white
  BorderColor #21ABAB
  DiamondBackgroundColor white
  DiamondBorderColor #21ABAB
}

start

repeat
  :ask for contact person's information;
  :enter data into SORMAS;
repeat while (more contacts?)

stop

@enduml

This process binds important capacities of the health office to the phone for a simple data inquiry. And the duration for the information depends on the amount of contacts. With more liberate lockdown rules the amount of contacts and thus the duration of the process increases drastically.

@startuml
skinparam sequence {
    ArrowColor #21ABAB
    ActorBorderColor #21ABAB
    LifeLineBorderColor #21ABAB
    ParticipantBorderColor #21ABAB
    ParticipantBackgroundColor white
    ParticipantFontColor #002F33
}

actor "Index Person" #FFFFFF
actor "Health Office" #21ABAB
database "Health Office Software" #FFFFFF

"Health Office" -> "Index Person" : Call and ask for contacts
loop for each contact person
    "Index Person" --> "Health Office" : Information about contact person
    "Health Office" -> "Health Office Software" : Store data
end
@enduml

## Digitalized Process

Instead of doing the tedious process of retrieving the data of :ContactPerson:contactpersons: manually over the phone, the Digitales Wartezimmer digitalizes the process of obtaining the list of contacts. It shifts the effort of the health office worker from retrieving the data over the phone towards the :IndexPerson: to send the data themselves.

@startuml
skinparam activity {
  ArrowColor #21ABAB
  StartColor #21ABAB
  BarColor #21ABAB
  EndColor #21ABAB
  BackgroundColor white
  BorderColor #21ABAB
  DiamondBackgroundColor white
  DiamondBorderColor #21ABAB
}

start
  :send link to Digitales Wartezimmer to Index person;
  :Index person enters contact data;
  :List of contacts is being transmitted to health office;
stop
@enduml

This frees up much needed ressources at the health office, improves the data quality and increases efficiency of the contact data retrieval process.

@startuml
skinparam sequence {
    ArrowColor #21ABAB
    ActorBorderColor #21ABAB
    LifeLineBorderColor #21ABAB
    ParticipantBorderColor #21ABAB
    ParticipantBackgroundColor white
    ParticipantFontColor #002F33
}

actor "Health Office" #21ABAB
actor "Index Person" #FFFFFF
participant "Digitales Wartezimmer"
database "Health Office Software" #FFFFFF

"Health Office" -> "Index Person" : Send link to Digitales Wartezimmer
deactivate "Health Office"

"Index Person" -> "Digitales Wartezimmer" : Enter data of contact persons
"Digitales Wartezimmer" -> "Health Office Software" : Send data directly
@enduml

## Just another Contact Tracing / Checkin app?

So how is this different from the multitude of other contact tracing or checkin apps?

A requirement for such apps to work is that the data of the contact has to be gathered before the notification about the positive test result and duty to send the data. What if a :IndexPerson: does not use a contact tracing apps, or did not check in to any establishments?

The Digitales Wartezimmer is the digital variant to the current process of the health office. Thus, it comes to play when a user does not have any contact tracing app installed. Or it can be perfectly complement the data retrieved from checkins at establishment with the personal contacts transmitted via the Digitales Wartezimmer. 

Furthermore, in contrast to other apps, the Digitales Wartezimmer does not primarily optimize the experience for the user, but targets the health office and the improvements of its processes.


