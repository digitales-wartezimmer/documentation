---
terms:
    IRIS: "Integration of Remote systems Into Sormas - a gateway for sending data into SORMAS instances of health offices"
    SORMAS: "Surveillance, Outbreak Response Management and Analysis System running as an incident management software at health offices"
    API: "Application programming interface to interact with an application programmatically"
    Frontend: "User facing part of the application"
    Backend: "Data layer of the application, which is accessed via the frontend"
    IndexPerson: "Person who has tested positive for COVID-19 and is required to tell the health office with whom they were in contact with"
    ContactPerson: "Person that had contact with a COVID-19 positive person and may also be infected"
title: Glossary
sidebar: false
---
 
# Glossary
 
<Glossary :terms="$frontmatter.terms" />