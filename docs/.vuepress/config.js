module.exports = {
    title: 'Digitales Wartezimmer',
    description: 'Documentation for Digitales Wartezimmer',
    base: '/documentation/',
    dest: 'public',
    head: [
        ['link', { rel: 'icon', href: '/favicon.ico' }]
    ],
    plugins: ['vuepress-plugin-child-toc', 'vuepress-plugin-glossary', 'vuepress-plugin-medium-zoom'],
    themeConfig: {
      displayAllHeaders: true,
      activeHeaderLinks: true,
      sidebar: {
        '/getting-started/': [
            {
                title: 'Getting Started',
                children: [
                    'motivation',
                    'users',
                    'health-office',
                    'developer'
                ],
                collapsable: false
            }
        ],
        '/concepts/': [
            {
                title: 'Concepts',
                children: [
                    'privacy',
                    'security',
                    'forms',
                    'workflows',
                    'connectors',
                    'external-data-sources'
                ],
                collapsable: false
            }
        ],
        '/guides/': [
            {
                title: 'Guides',
                children: [
                    'authentication',
                    'create-dw-link',
                    'workflow-configuration',
                    'connector-configuration',
                    'ssh-connection',
                    'db-connection',
                    'analytics'
                ],
                collapsable: false
            }
        ],
        '/tech/': [
            {
                title: 'Technical Documentation',
                children: [
                    'environments',
                    'api-documentation',
                    'monitoring',
                    'techstack',
                    'infrastructure',
                    'deployment',
                    'user-management',
                    'client-authentication',
                    'testing',
                    'codebase'
                ],
                collapsable: false
            }
        ],
        '/': 'auto'
      },
      nav: [
        { text: 'Getting Started', link: '/getting-started/' },
        { text: 'Concepts', link: '/concepts/' },
        { text: 'Guides', link: '/guides/' },
        { text: 'Technical Documentation', link: '/tech/' },
        { text: 'Glossary', link: '/glossary' },
      ],
      logo: '/icon.png'
    },
    markdown: {
      extendMarkdown: md => {
        md.use(require('markdown-it-footnote'))
        md.use(require('markdown-it-plantuml'))
      }
    }
  }
  