---
title: How to create a Digitales Wartezimmer Link
---

# How to create a Digitales Wartezimmer Link

How should an :IndexPerson: get the correct link to the form on Digitales Wartezimmer where the data should be submitted. 

## Form Urls

Please refer to the [Forms](../concepts/forms) overview to get a list of the forms and the corresponding urls. 

You can supply the `case-id` url parameter to have the Case ID prefilled where available, like so `https://digitales-wartezimmer.org/index-case?case-id=1234-abcd`

## Link Generator

::: tip PLANNED
This feature is planned but not implemented yet
:::

You can also use the Link Generator form, to generate the url with a supplied Case ID.
