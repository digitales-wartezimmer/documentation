---
title: Connector Configuration
---

# Connector Configuration

This page outlines how [Connectors](../concepts/connectors) can be managed.

## Creating a Connector

Since the connectors hold sensitive data, some of the data (currently only the password) are being encrypted on the application layer before being stored in the database and decrypted upon retrieval. This should prevent abuse of the credentials if an attacker should gain access to the database.

Ideally the connector is also created by the health office directly via the API, so the password does not have to be sent on a different channel. This can be done via the `/connectors` endpoint of the API and with a Health office specific User that has the [role](../tech/user-management) `HealthOffice`

@startuml

skinparam sequence {
    ArrowColor #21ABAB
    ActorBorderColor #21ABAB
    LifeLineBorderColor #21ABAB
    ParticipantBorderColor #21ABAB
    ParticipantBackgroundColor white
    ParticipantFontColor #002F33
}

actor "Health Office" #21ABAB
actor "User" #21ABAB
participant "API"
database DB #FFFFFF
database SORMAS #FFFFFF

group Adding of Connector data
    "Health Office" -> "API" : POST Request /connectors with credentials in request body
    "API" --> "API" : Encryption of credentials with local DB secret
    "API" -> "DB" : Store encrypted credentials in database
    "API" --> "Health Office" : Success - Response
end

...

group Usage of Connector data
    "User" -> "API" : Sends data
    "API" -> "DB" : Retrieve relevant connector configuration
    "DB" --> "API" : encrypted credentials
    "API" --> "API" : decrypt credentials with local DB secret
    "API" -> "SORMAS" : Send data with correct credentials
    "API" --> "User" : Sucess - Response
end
@enduml

## Retrieving a Connector

Connectors can be searched and listed when connected to the database. Therefore [connect to the database](./db-connection).

Then run the query command and supply the relevant parameters that you want to filter by

```
db.connectors.find({ "code": "1.2.3.4", "service": "sormas" });
```

## Removing a Connector

A connector needs to be removed manually with direct database access. First [connect to the database](./db-connection).

Then delete the connector that can be identified by the code and the service name

```
db.connectors.remove({ "code": "1.2.3.4", "service": "sormas" });
```

## Updating a Connector 

Since the password is encrypted on the application layer, updating a connector is only possible for unencrypted fields via the MongoDB CLI. Therefore, it usually is the better approach to [remove the relevant connector](#removing-a-connector) and then [create it again](#creating-a-connector).