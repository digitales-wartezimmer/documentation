---
title: SSH Connection
---


# SSH Connection

The server can be connected to via SSH. However, the SSH keys of the machine used to connect need first to be stored on the server to be able to authenticate.

:::tip
It is recommended to read about the [server infrastructure](../tech/infrastructure) first.
:::

## Connect to the Front Server

The front server can be connected directly via ssh.

```bash
ssh dw@dwz-front.bitkomplex.de
```

To connect as root user specify the root user.

```bash
ssh root@dwz-front.bitkomplex.de
```

## Connect to the Back Server

The back server is not available over the internet directly. First a connection to the front server must be established and then from there a connection to the back server can be created

```bash
ssh -J root@dwz-front.bitkomplex.de root@dwz-back.bitkomplex.de
```

To connect as root user specify the root user.

```bash
ssh -J dw@dwz-front.bitkomplex.de dw@dwz-back.bitkomplex.de
```