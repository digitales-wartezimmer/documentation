---
title: Analytics
---

# Analytics

Currently we do not track any user information or events directly to protect the end users data. Therefore, only the server logs are available to get an estimate of user numbers.

To analyze these service logs we utilize [GoAccess](https://goaccess.io/).

### 1. Install GoAccess

Install GoAccess on your local machine. Refer to [GoAccess' documentation](https://goaccess.io/get-started) for your operating system.

### 2. Identify path of access logs

[Establish a SSH Connection](ssh-connection) to the server and extract file path of logs.

```bash
docker inspect --format='{{.LogPath}}' nginx-proxy
```

This results in a path which probably resembles `/var/lib/docker/containers/09db251e8186e7/09db251e8186e7-json.log`. Copy this path to your clipboard.

### 3. Move file to your local machine

Close the ssh session and run the following command with the file path from before to copy the log file to your local machine

```bash
scp dwz-front.bitkomplex.de:/var/lib/docker/containers/09db251e8186e7/09db251e8186e7-json.log ~/Downloads/
```

To enable an easier file handling rename the file

```bash
mv ~/Downloads/09db251e8186e7/09db251e8186e7-json.log access.log
```

### 4. Remove incorrect or problematic lines

Since this file contains logs for all services running on the server it may include error messages. GoAccess has trouble parsing them, so this file first needs to be stripped from such problematic lines.

```bash
sed -i'' -e '/^.*forego.*$/d' access.log
sed -i'' -e '/^.*dockergen.1.*$/d' access.log
sed -i'' -e '/^.*Custom dhparam\.pem file found, generation skipped.*$/d' access.log
```

### 5. (Optional) remove logs from other applications

To keep the statistics clean from other applications running on the same hosts, those logs should also be removed.

```bash
sed -i'' -e '/^.*staging.*$/d' access.log
sed -i'' -e '/^.*localhost.*$/d' access.log
sed -i'' -e '/^.*uptimerobot.*$/d' access.log
sed -i'' -e '/^.*content\.digitales-wartezimmer.*$/d' access.log
sed -i'' -e '/^.*api\.digitales-wartezimmer.*$/d' access.log
```

### 6. Generate HTML analytics report

Finally, the HTML report can be generated with GoAccess.

```bash
goaccess access.log --log-format='%^[ %^[0%v %h %^[%d:%t %^] %^"%m %U %H\\\"  %s %b %^"%R\\\" %^"%u\\\" %^' --date-format=%d/%b/%Y --time-format=%T --invalid-requests=error.txt -o report.html
```

This results in a file `report.html` which can be opened with any browser.
Should an error arise the problematic lines of the logs can be found in `error.txt`. Try to remove those lines by adjusting and reruninng the exclusion commands from [Step 4](#_4-remove-incorrect-or-problematic-lines).
