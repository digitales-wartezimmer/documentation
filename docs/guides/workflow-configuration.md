---
title: Workflow Configuration
---


# Workflow Configuration

This guide illustrates how [Workflows](../concepts/workflows) can be managed

## Retrieving Workflow Configurations

Retrieving or Listing workflows can be done via the API with the endpoints `/workflowConfigurations/all` to get a list of all configured workflows, or via `/workflowConfiguration` that retrieves the workflows for a given postal code.

## Creating Workflow Configurations

The creation of new workflows has to be made via direct Database Access. First [connect to the database](./db-connection).

Then you can add a workflow configuration for a health office with the following MongoDB command:

```
db.workflowconfigurations.insertOne({ code: "1.2.3.4", name: "demo-health-office", workflows: { registerAsContact: "email", travelReturn: "sormas", indexCase: "iris" } });
```

## Updating Workflow Configurations

You can also update specific workflow configurations.  First [connect to the database](./db-connection) as well.

Then retrieve the id for the workflow configuration that you want to edit

```
db.workflowconfigurations.find({ code: "1.2.3.4" });
```

Then run the following command, while supplying the id of the health office you wish to update in the second argument.

```
db.workflowconfigurations.update({ "_id": ObjectId("5fbe8e50a9e0bd330a09e606") },{ $set: { "workflows": { "registerAsContact": "email", "travelReturn": "email", "indexCase": "sormas" }}});
```

## Deleting Workflow Configurations

Deleting a Workflow configuration works the same. First [connect to the database](./db-connection) and retrieve the id as above.

Then run the delete command.

```
db.workflowconfigurations.remove({ "_id": ObjectId("5fbe8e50a9e0bd330a09e606") });
```