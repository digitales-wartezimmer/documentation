---
title: API Authentication
---

# API Authentication

To prevent abuse of our REST - :API:, we enforce clients to authenticate themselves.

### Available Roles

There exist different roles with different permissions, to prevent access to resources that are not necessary for the intended use. Which resources are available for which role can be found in the [API-Documentation](../tech/api-documentation). As a general rule of thumb, the roles are listed below in ascending order of priviliges.

| Role | Description |
|------|-------------|
| Monitor      | Role that is allowed to request health endpoints for automatic checking of the status of the :API: |
| User     | Role for an end user or client that is used directly by end users | 
| HealthOffice     | Role intended for use of health offices, to configure data related to a specific health office | 
| Admin     | Admin Role to maintain and configure the application | 


### Obtaining an API - Token

To obtain an API Token that identifies a user and acts as a password, please contact the team. Usually a token is a 32 character string that can contain lower and upper case letters and numbers, e.g. `a7qHCeqzEG2kk1H2UIO2UZopLbzBnNGV`.

## API-Key Authentication

To interact with the :API: a client user needs to add an Authorization header with the key `Api-Key` to each request that also contains the token, e.g. `Authorization: Api-Key a7qHCeqzEG2kk1H2UIO2UZopLbzBnNGV`. With the passed token the requesting user can be authenticated and check if it fulfills the required roles to access the requested ressource.

@startuml
skinparam sequence {
    ArrowColor #21ABAB
    ActorBorderColor #21ABAB
    LifeLineBorderColor #21ABAB
    ParticipantBorderColor #21ABAB
    ParticipantBackgroundColor white
    ParticipantFontColor #002F33
}

actor "Client" #21ABAB
participant "API"

"Client" -> "API" : Request with Header 'Api-Key my_token_here'
"API" --> "Client": Response
...
"Client" -> "API" : Request with invalid Header 'Api-Key'
"API" --> "Client": Unauthorized Response
@enduml

This method is usually used when interacting directly with the API, which is mostly done by admins, health offices and monitors, but not users.

## Session Authentication

Instead of sending the :API: token on each request a client can also use session based authentication. This way the user can authenticate with the session cookie, and is not sending the :API: token every time to avoid it being stolen or misused.

The client requests a session cookie by sending a request with the :API: token to the endpoint `/auth/authenticate` and received the session cookie. This session cookie then needs to be sent with subsequent requests. Currently session cookies expire after 30 minutes and a new session cookie needs to be requested in that case.

@startuml
skinparam sequence {
    ArrowColor #21ABAB
    ActorBorderColor #21ABAB
    LifeLineBorderColor #21ABAB
    ParticipantBorderColor #21ABAB
    ParticipantBackgroundColor white
    ParticipantFontColor #002F33
}

actor "Client" #21ABAB
participant "API"

"Client" -> "API" : Request /auth/authenticate with token in request body
"API" --> "Client": Response including Session Cookie
...
"Client" -> "API" : Request with Session Cookie
"API" --> "Client": Response
...
"Client" -> "API" : Request of any API resource without (e.g. expired) Session Cookie
"API" -> "Client" : Unauthorized Response
@enduml

This session based authentication method is typically used by end users. How this is implemented in the :Frontend:frontend: of Digitales Wartezimmer can be seen in the technical documentation of the [Client Authentication](../tech/client-authentication),