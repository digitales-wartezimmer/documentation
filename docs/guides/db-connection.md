---
title: DB Connection
---


# DB Connection

When you should feel the need to edit objects in the database directly and manually you have to connect to the database instance. 

### Establish SSH Connection

First, establish an SSH connection to the front server. This is outlined in the [SSH Connection Guide](ssh-connection).

## MongoDB

You can connect to the MongoDB instance directly from the front server, although it is running on the back server

### Staging Database

Login to the staging database with the staging user

```bash
mongo mongodb://10.0.4.11:27017/dw-staging -u dw-staging
```

You will be prompted for a password, which is stored in the password manager.

### Production Database

Login to the production database with the production user

```bash
mongo mongodb://10.0.4.11:27017/dw -u dw
```

You will be prompted for a password, which is stored in the password manager.

### Admin Access

Access the whole mongo instance with the admin user

```bash
mongo mongodb://10.0.4.11:27017 -u dw-admin
```

You will be prompted for a password, which is stored in the password manager.

## Redis

The Redis instance does not differentiate between environments, you can use the same command for both environments to connect to Redis

```bash
redis-cli -h 10.0.4.11 -p 6379
```